package com.s3corp.cafe.threading.tasks;

import org.apache.log4j.Logger;

import com.s3corp.cafe.threading.stores.CafeSteps;

public class AllProcessTask implements Runnable {

	private static final Logger LOGGER = Logger.getLogger(AllProcessTask.class);
	
	private CafeSteps cafeStep;

	public AllProcessTask(CafeSteps cafeStep) {
		this.cafeStep = cafeStep;
	}

	@Override
	public void run() {
		
//		LOGGER.info("--------start a new request " + Thread.currentThread().getName());
		
		String orderdCafe;
		try {
			orderdCafe = cafeStep.requestOrder();
			cafeStep.recordOrder(orderdCafe);
			cafeStep.comeToTheBar(orderdCafe);
			cafeStep.preparationCafe(orderdCafe);
			cafeStep.bringBackToCust(orderdCafe);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		LOGGER.info("--------end a the request " + Thread.currentThread().getName());
	}

}
