package com.s3corp.cafe.threading;

public interface IMessages {
	
	public static final String CUST_REQUEST = "Customer [%s] REQUEST order Milano cafe [%s]";
	
	public static final String WAITER_RECORD = "Waiter [%s] RECORD order [%s]" ;
	
	public static final String WAITER_COME_TO_BAR = "Waiter [%s] COME to the bar to request making [%s]";
	
	public static final String WAITER_BRING_BACK_CUST = "Waiter [%s] BRING back the [%s] to the cust ";
	
	public static final String BARTENDER_MAKING_CAFE = "Bartender [%s] is MAKING the [%s]";

}
