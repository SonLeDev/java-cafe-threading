package com.s3corp.cafe.threading;

import org.apache.log4j.Level;

public interface IConfigApp {
	public static final Level LOG_LEVEL = Level.INFO;
}
