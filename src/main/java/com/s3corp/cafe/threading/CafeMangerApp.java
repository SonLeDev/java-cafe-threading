package com.s3corp.cafe.threading;

import com.s3corp.aop.Transaction;
import com.s3corp.cafe.threading.stores.ICafeStore;
import com.s3corp.cafe.threading.stores.orginized.CafeOrginizedBuilder;

public class CafeMangerApp {

	@Transaction
	public static void main(String[] args) {

		int nRequests = 1;
		ICafeStore cafeStore = null;
		
		// cafeStore = new CafeSingleFlows();
		// cafeStore = new CafeMutiThreadFlowsPooled();

//		cafeStore = new CafeMutiThreadFlowsUnlimited();
		
//		 cafeStore = new CafeMutiThreadFlowsPooled(10);

		cafeStore = CafeOrginizedBuilder.getInstance()
				.setNBartenders(7)
				.setNWaiters(3)
				.setNRequests(nRequests)
				.build();
		cafeStore.serveCafe(nRequests);

	}

}
