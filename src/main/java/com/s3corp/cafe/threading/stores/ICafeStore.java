package com.s3corp.cafe.threading.stores;

import com.s3corp.aop.Transaction;

public interface ICafeStore {
	@Transaction
	void serveCafe(int nRequest);
}
