package com.s3corp.cafe.threading.stores.orginized;

import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class CustomerRequestPool extends CafePools<Map, Map> implements Runnable {

	private static final Logger LOGGER = Logger.getLogger(CustomerRequestPool.class);

	private long serializeId;
	private int nRequest = 0;

	public CustomerRequestPool(int nRequest) {

		this.nRequest = nRequest;

		this.inputReqQueue = IQueues.waiterToCustQueue;
		this.outputReqQueue = IQueues.custToWaiterQueue;

		executorService = Executors.newSingleThreadExecutor();
	}

	public void setNRequests(int nRequests) {
		this.nRequest = nRequests;
	}

	@Override
	public void run() {
		try {
			for (int i = 0; i < nRequest; i++) {
				executorService.submit(() -> {
					serializeId = Calendar.getInstance().getTimeInMillis();
					try {

						Map wantedCafe = this.getCafeSteps().requestOrder(serializeId);
						outputReqQueue.put(wantedCafe);

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					synchronized (IConfigThreads.SPEAKER_NOTIFY) {
						IConfigThreads.SPEAKER_NOTIFY.notify();
						LOGGER.debug("-----SPEACKER NOTIFY notify ......");
					}
				});
			}
		} finally {
			if (executorService != null)
				executorService.shutdown();
		}
		if (executorService != null) {
			try {
				executorService.awaitTermination(1, TimeUnit.MINUTES);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Check whether all tasks are finished
			if (executorService.isTerminated())
				LOGGER.warn("All tasks finished");
			else
				LOGGER.warn("At least one task is still running");
		}
	}

}
