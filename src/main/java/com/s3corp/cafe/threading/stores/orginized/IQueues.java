package com.s3corp.cafe.threading.stores.orginized;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by sonle on 4/18/18.
 */
public final class IQueues {

    static final BlockingQueue custToWaiterQueue = new LinkedBlockingQueue<>();

    static final BlockingQueue waiterToCustQueue = new LinkedBlockingQueue<>();

    static final BlockingQueue waiterToBarQueue = new LinkedBlockingQueue<>();

    static final BlockingQueue barToWaiterQueue = new LinkedBlockingQueue<>();

}
