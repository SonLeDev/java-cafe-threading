package com.s3corp.cafe.threading.stores;

import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;

import com.s3corp.cafe.threading.tasks.AllProcessTask;

public class CafeSingleFlows implements ICafeStore{
	
	private static final Logger LOGGER = Logger.getLogger(CafeSingleFlows.class);
	
	StopWatch stopWatch = new StopWatch();
	CafeSteps cafeSteps = new CafeSteps();

	public void serveCafe(int numOfRequest) {
		
		stopWatch.start();
		for (int i = 0; i < numOfRequest; i++) {
			AllProcessTask task = new AllProcessTask(cafeSteps);
			task.run();
		}
		stopWatch.stop();
		LOGGER.info("============> Total time for "+ numOfRequest + " is : " + stopWatch.getTime()/60 + "s");

	}
}
