package com.s3corp.cafe.threading.stores.orginized;

import com.s3corp.aop.Transaction;
import com.s3corp.cafe.threading.stores.CafeOrginized;
import com.s3corp.cafe.threading.stores.ICafeStore;

public class CafeOrginizedBuilder {
	
	private CafeOrginizedBuilder() {}
	
	private static CafeOrginizedBuilder instance = new CafeOrginizedBuilder();
	
	public static CafeOrginizedBuilder getInstance() {return instance;}
	
	private int nWaiters = 1;
	private int nBartenders = 1;
	private int nRequests = 1;
	
	public CafeOrginizedBuilder setNWaiters(int nWaiters) {
		this.nWaiters = nWaiters;
		return this;
	}
	
	public CafeOrginizedBuilder setNBartenders(int nBartenders) {
		this.nBartenders = nBartenders;
		return this;
	}
	
	public CafeOrginizedBuilder setNRequests(int nRequest) {
		this.nRequests = nRequest;
		return this;
	}
	
	@Transaction
	public ICafeStore build() {
		CafeOrginized cafeOrginized = new CafeOrginized(nWaiters,nBartenders,nRequests);
		return cafeOrginized;
	}
}
