package com.s3corp.cafe.threading.stores.orginized;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.s3corp.cafe.threading.stores.CafeSteps;

public class CafePools<T, U> {

	protected ExecutorService executorService;

	protected BlockingQueue<T> inputReqQueue;
	protected BlockingQueue<U> outputReqQueue;
	protected BlockingQueue<T> inputRespQueue;
	protected BlockingQueue<U> outputRespQueue;

	protected int nThread = IConfigThreads.NUM_OF_BARTENDER;
	private static final CafeSteps cafeSteps = new CafeSteps();

	public CafePools() {
		executorService = Executors.newFixedThreadPool(nThread);
	}

	public CafePools(int nThread) {
		executorService = Executors.newFixedThreadPool(nThread);
		this.nThread = nThread;
	}

	protected CafeSteps getCafeSteps() {
		return CafePools.cafeSteps;
	}

}
