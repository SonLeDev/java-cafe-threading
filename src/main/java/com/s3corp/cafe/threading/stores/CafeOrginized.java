package com.s3corp.cafe.threading.stores;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.s3corp.aop.Transaction;
import com.s3corp.cafe.threading.stores.orginized.BartenderPoolManager;
import com.s3corp.cafe.threading.stores.orginized.CustomerRequestPool;
import com.s3corp.cafe.threading.stores.orginized.WaiterPoolManager;


public class CafeOrginized implements ICafeStore {

	private int nWaiters = 5;
	private int nBartenders = 5;
	private int nRequest = 10;
	private final WaiterPoolManager waiters ;
	private final BartenderPoolManager bartenderPool ;
	private final CustomerRequestPool customers ;

	public CafeOrginized() {
		waiters = new WaiterPoolManager(nWaiters);
		bartenderPool = new BartenderPoolManager(nBartenders);
		customers = new CustomerRequestPool(nRequest);
	}
	
	public CafeOrginized(int nWaiters,int nBartenders,int nRequest) {
		
		this.nWaiters = nWaiters;
		this.nBartenders = nBartenders;
		this.nRequest = nRequest;
		
		waiters = new WaiterPoolManager(nWaiters);
		bartenderPool = new BartenderPoolManager(nBartenders);
		customers = new CustomerRequestPool(nRequest);
	}
	
	@Transaction
	@Override
	public void serveCafe(int nRequests) {
		
		customers.setNRequests(nRequests);
		
		ExecutorService executorService = Executors.newFixedThreadPool(3);
		
		executorService.submit(customers);
		
		executorService.submit(bartenderPool);

		executorService.submit(waiters);


	}

}
