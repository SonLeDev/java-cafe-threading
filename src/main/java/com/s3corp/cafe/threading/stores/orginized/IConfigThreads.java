package com.s3corp.cafe.threading.stores.orginized;

/**
 * Created by sonle on 4/18/18.
 */
public interface IConfigThreads {
    static final int NUM_OF_WAITER = 3;
    static final int NUM_OF_BARTENDER = 10;
    static final SpeakerNotify SPEAKER_NOTIFY = new SpeakerNotify();
}
