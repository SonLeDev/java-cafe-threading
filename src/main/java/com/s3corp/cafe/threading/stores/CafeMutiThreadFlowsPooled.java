package com.s3corp.cafe.threading.stores;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;

import com.s3corp.cafe.threading.tasks.AllProcessTask;

public class CafeMutiThreadFlowsPooled implements ICafeStore {

	private static final Logger LOGGER = Logger.getLogger(CafeMutiThreadFlowsPooled.class);

	private int nThread = 0;
	
	public CafeMutiThreadFlowsPooled(int nThreads) {
		this.nThread = nThreads;
	}
	
	public void serveCafe(int numOfRequest) {

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		CafeSteps cafeStep = new CafeSteps();
		
		ExecutorService executorService = null;
		executorService = Executors.newFixedThreadPool(nThread);

		try {
			for (int i = 0; i < numOfRequest; i++) {
				executorService.submit(new AllProcessTask(cafeStep));
			}
		} finally {
			if (executorService != null) {
				//LOGGER.info("================ executorService.shutdown() ......");
				executorService.shutdown();
			}
			if(executorService.isTerminated()) {
				stopWatch.stop();
				LOGGER.info("================ cost time : " + stopWatch.getTime()/3600 + "m");
			}
				
		}

	}

}
