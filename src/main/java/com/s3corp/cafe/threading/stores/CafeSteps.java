package com.s3corp.cafe.threading.stores;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;

import com.s3corp.cafe.threading.IMessages;

public class CafeSteps {

	private static final Logger LOGGER = Logger.getLogger(CafeSteps.class);

	private static final int ESLAPSE_TIME_STEP = 100;
	private static final int RANGE_TYPE_CAFE = 5;
	private static final int REQUEST_ORDER_TIME = 2;
	private static final int RECORD_ORDER_TIME = 3;
	private static final int MOVING_TIME = 5;
	private static final int MAKING_TIME = 45;


	private Random randomCafeType = new Random();
	private DateFormat df = new SimpleDateFormat("HH:mm:ss");

	public String requestOrder() throws InterruptedException {

		String cafeInfo = new StringBuilder("Cafe ").append("_").append(randomCafeType.nextInt(RANGE_TYPE_CAFE)).append("_")
				.append(df.format(new Date())).toString();

		LOGGER.info(String.format(IMessages.CUST_REQUEST, Thread.currentThread().getName(), cafeInfo));

		Thread.sleep(RECORD_ORDER_TIME * ESLAPSE_TIME_STEP);

		return cafeInfo;
	}

	public void recordOrder(String orderedCafeInfo) throws InterruptedException {

		LOGGER.info(String.format(IMessages.WAITER_RECORD, Thread.currentThread().getName(), orderedCafeInfo));

		Thread.sleep(RECORD_ORDER_TIME * ESLAPSE_TIME_STEP);
	}

	public void comeToTheBar() throws InterruptedException {

		LOGGER.info(String.format(IMessages.WAITER_COME_TO_BAR, Thread.currentThread().getName(), ""));

		Thread.sleep(MOVING_TIME * ESLAPSE_TIME_STEP);
	}

	public void bringBackToCust() throws InterruptedException {

		LOGGER.info(String.format(IMessages.WAITER_BRING_BACK_CUST, Thread.currentThread(), ""));

		Thread.sleep(MOVING_TIME * ESLAPSE_TIME_STEP);
	}

	public void preparationCafe() throws InterruptedException {

		LOGGER.info(String.format(IMessages.BARTENDER_MAKING_CAFE, Thread.currentThread().getName(), ""));

		Thread.sleep(MAKING_TIME * ESLAPSE_TIME_STEP);
	}

	public void comeToTheBar(String orderdCafe) throws InterruptedException {

		LOGGER.info(String.format(IMessages.WAITER_COME_TO_BAR, Thread.currentThread().getName(), orderdCafe));

		Thread.sleep(MOVING_TIME * ESLAPSE_TIME_STEP);
	}

	public void preparationCafe(String orderdCafe) throws InterruptedException {
		
		LOGGER.info(String.format(IMessages.BARTENDER_MAKING_CAFE, Thread.currentThread().getName(), orderdCafe));

		Thread.sleep(MAKING_TIME * ESLAPSE_TIME_STEP);
	}

	public void bringBackToCust(String orderdCafe) throws InterruptedException {
		
		LOGGER.info(String.format(IMessages.WAITER_BRING_BACK_CUST, Thread.currentThread().getName(),orderdCafe));
		
		Thread.sleep(MOVING_TIME * ESLAPSE_TIME_STEP);
	}

	public Map requestOrder(long serializeId) throws InterruptedException {

		String cafeInfo = new StringBuilder("Cafe number ").append("_").append(randomCafeType.nextInt(RANGE_TYPE_CAFE))
				.append("_").append(serializeId).toString();

		Map record = new HashMap<>();
		record.put(serializeId, cafeInfo);
		
		LOGGER.info(String.format(IMessages.CUST_REQUEST, Thread.currentThread().getName(),cafeInfo));
		
		Thread.sleep(REQUEST_ORDER_TIME * ESLAPSE_TIME_STEP);
		LOGGER.debug("..done..");
		return record;
	}

	public void recordOrder(Map<Long, String> takeRequestOrder) {
		// TODO Auto-generated method stub

	}

}
