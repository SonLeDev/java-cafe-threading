package com.s3corp.cafe.threading.stores.orginized;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class WaiterPoolManager extends CafePools<Map, Map> implements Runnable {

	private static final Logger LOGGER = Logger.getLogger(WaiterPoolManager.class);

	private int nThread = IConfigThreads.NUM_OF_WAITER;

	private boolean isRunning = true;

	public WaiterPoolManager() {

		this.inputReqQueue = IQueues.custToWaiterQueue; // 1
		this.outputReqQueue = IQueues.waiterToBarQueue; // 2
		this.inputRespQueue = IQueues.barToWaiterQueue; // 3
		this.outputRespQueue = IQueues.waiterToCustQueue; // 4

	}

	public WaiterPoolManager(int nWaiters) {
		super(nWaiters);
		this.inputReqQueue = IQueues.custToWaiterQueue; // 1
		this.outputReqQueue = IQueues.waiterToBarQueue; // 2
		this.inputRespQueue = IQueues.barToWaiterQueue; // 3
		this.outputRespQueue = IQueues.waiterToCustQueue; // 4
	}

	@Override
	public void run() {

		while (isRunning) {
			// TODO : this thread should be wait notification from customer or bartender
			try {
				synchronized (IConfigThreads.SPEAKER_NOTIFY) {
					LOGGER.debug("Waiter is waiting for notify...");
					IConfigThreads.SPEAKER_NOTIFY.wait();
					LOGGER.debug("Waiter is reentry to work...");
				}

			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			// fork a guy to server the customer
			executorService.submit(() -> {
				String orderedcafe = "";
				try {
					// wait to get request order from customer about 1s, if not, do the others task
					Map<Long, String> takeRequestOrder = inputReqQueue.poll(1, TimeUnit.MICROSECONDS);
					if (!takeRequestOrder.isEmpty()) {

						LOGGER.debug(String.format("==>>>Waiter [%s] start his ORDER task",
								Thread.currentThread().getName()));

						orderedcafe = takeRequestOrder.values().stream().findFirst().get();
						this.getCafeSteps().recordOrder(orderedcafe);
						this.getCafeSteps().comeToTheBar(orderedcafe);
						outputReqQueue.put(takeRequestOrder);
					}
					// There are nothing to do, go back to the pool
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				LOGGER.debug(
						String.format("Waiter [%s] finished his ORDER task /////", Thread.currentThread().getName()));
			});

			// fork another guy to bring back cafe to the customer
			executorService.submit(() -> {
				try {
					// wait to get cafe from bartender
					Map<Long, String> takeRespOrder = inputRespQueue.poll(1, TimeUnit.MICROSECONDS);
					if (!takeRespOrder.isEmpty()) {
						LOGGER.debug(String.format("==>>>Waiter [%s] start his BRING task",
								Thread.currentThread().getName()));
						String cafeInfo = takeRespOrder.values().stream().findFirst().get();
						this.getCafeSteps().bringBackToCust(cafeInfo);
						outputRespQueue.put(takeRespOrder);
					}
					// There are nothing to do, go back to the pool
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				LOGGER.debug(
						String.format("Waiter [%s] finished his BRING task /////", Thread.currentThread().getName()));
			});

		}

	}

}
