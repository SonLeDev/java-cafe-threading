package com.s3corp.cafe.threading.stores;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.s3corp.cafe.threading.tasks.AllProcessTask;

public class CafeMutiThreadFlowsUnlimited implements ICafeStore {

	private static final Logger LOGGER = Logger.getLogger(CafeMutiThreadFlowsUnlimited.class);

	public void serveCafe(int nRequests) {
		ExecutorService executorService = null;
		CafeSteps cafeSteps = new CafeSteps();
		int i = 0;
		for (; i < nRequests; i++) {
			executorService = Executors.newSingleThreadExecutor();
			try {
				executorService.submit(new AllProcessTask(cafeSteps));
			} finally {
				if (executorService != null)
					executorService.shutdown();
			}

		}

	}

}
