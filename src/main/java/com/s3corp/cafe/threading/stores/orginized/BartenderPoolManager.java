package com.s3corp.cafe.threading.stores.orginized;

import java.util.Map;

import org.apache.log4j.Logger;

public class BartenderPoolManager extends CafePools<Map, Map> implements Runnable {

	private static final Logger LOGGER = Logger.getLogger(BartenderPoolManager.class);

	public BartenderPoolManager() {
		super(IConfigThreads.NUM_OF_BARTENDER);
		this.inputReqQueue = IQueues.waiterToBarQueue;
		this.outputRespQueue = IQueues.barToWaiterQueue;
	}

	public BartenderPoolManager(int nBartenders) {
		super(nBartenders);
		this.inputReqQueue = IQueues.waiterToBarQueue;
		this.outputRespQueue = IQueues.barToWaiterQueue;
	}

	@Override
	public void run() {

		/**
		 * The all bartenders should were initialed at the first time and then
		 * wait/sleep for inputReqQueue
		 */
		for (int i = 0; i < nThread; i++) {
			executorService.submit(() -> {
				while (true) {
					String orderedCafe = "";
					try {
						// wait to get request order from waiter
						Map<Long, String> takeRequestOrder = inputReqQueue.take();
						if (!takeRequestOrder.isEmpty()) {
							orderedCafe = takeRequestOrder.values().stream().findFirst().get();
							this.getCafeSteps().preparationCafe(orderedCafe);

							// response cafe to waiter
							outputRespQueue.put(takeRequestOrder);
							synchronized (IConfigThreads.SPEAKER_NOTIFY) {
								IConfigThreads.SPEAKER_NOTIFY.notify();
								LOGGER.debug("-----SPEACKER NOTIFY notify ......");
							}

						}
						// goto s

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			});
		}
	}
}
