package com.s3corp.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class TransactionAspect {

	private static final Logger LOGGER = Logger.getLogger(TransactionAspect.class);

	@Pointcut("@annotation(Transaction)")
	public void atTransaction(Transaction transaction) {
	}

	@Around("atTransaction(Transaction)")
	public Object aroundTransaction(ProceedingJoinPoint pjp, Transaction secured) throws Throwable {
		System.out.println("AAAAAAAAAAAAAAA Around to do something ..........");
		LOGGER.info("AAAAAAAAAAAAAAAAAA");
		return null;
	}

	@Before("atTransaction(Transaction)")
	public void beforeTransaction(JoinPoint joinPoint) {
		LOGGER.info("AAAAAAAAAAAAAAAAAA");
		System.out.println("AAAAAAAAAAAAAAA Before to do something ..........");
	}

	@Pointcut("execution(* *(..))")
	public void myTraceCall() {
	}

	@Around("com.s3corp.aop.TransactionAspect.myTraceCall()")
	public Object myTrace(ProceedingJoinPoint joinPoint) {
		System.out.println("AAAAAAAAAAAAAAA @Around to do something ..........");
		return null;
	}

}
